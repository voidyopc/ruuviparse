# ruuviparse

Collection of scripts to deal with [ruuvi tags](https://ruuvi.com) on
the command line. Listen to, read, parse, log and plot measurements.

The RuuviTag sensors measure temperature, air humidity, air pressure and
acceleration. They use bluetooth low energy to broadcast the
measurements. 

The measurements are [encoded](notes) in the payload of the packets and
need to be decoded to become human readable.


# collect measurements

In order to dump the activity of the bt interface, run in terminal 1 the
`hcitool` (sudo may be needed):

	hcitool lescan

(adding the `--duplicates` option writes a lot while without
`--duplicates`, looks like scan is stale. Still need to figure out what
makes more sense in general. TODO)

In terminal 2, listen to beacons, parse them and write them to the log
with

	sudo btmon --time --date | ./listen

This writes to a log file, e.g. `logs/ruuvi-2021-06-21` lines of the
following form  containing a timestamp, signal strength, sensor readings
and the device MAC address: 

	2021-06-21T08:54:55 -68 27.12 55.0 1000.64 2917 FF:AB:CD:01:EE:11


# view

- `./plot` and `./plotX11` show a graph of today's recorded measurements
- `./recent` prints the most recent reading(s) to the terminal

- `macalias` allows for assigning aliases to the individual tags. The
  aliases are configured in the `aliases` file and the `macalias` script
  can convert from a MAC to a name and backwards, i.e. from a pattern to
  a MAC address. 

# parsing the beacon data

The `listen` utility is an awk script that parses the beacons parts.
From `btmon`'s output, it extracts the ruuvi tags' beacon payload, the
address of the device, and the signal strength.

The bacon payload is parsed with the `parse` utility to extract the
measurements transmitted by the device. Currently it supports only
["datafomat_03"](https://github.com/ruuvi/ruuvi-sensor-protocols/blob/master/dataformat_03.md);
and "dataformat_05".


