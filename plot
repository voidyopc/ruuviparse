#!/bin/sh

ts=`date +%F`
w=`tput cols`
h=$((w / 4))
size=$w,$h
xrange=
yrange=
layout=3,2


while getopts m:f:d:s:l:x:y:n:phTPSHBA o; do
	case "$o" in
	m)	# use most recent file for partucular MAC
		mac="$OPTARG"
		case $mac in
			les*|lataniers*|*bleus) mac="FF:6B:99:A9:49:11" ;;
			riad*|clos*|*arts) mac="FE:D2:13:F2:EA:C1" ;;
			??:??:??:??:??:??) mac=$mac ;;
			*) echo "This is neither a valid MAC nor a known device name"; exit; ;;
		esac
		file=`ls -t ~/dev/ruuviparse/logs/ruuvi-$mac-* | sed 1q` ;;
	p)	# use in a pipe, i.e. use stdin
		file="-" ;;
	f)	# use particular file
		file="OPTARG" ;;
	d)	# date different than today, as would be valid input for `date -d`,
		# e.g. 'two days ago' or '-10 weeks' or 'yesterday'
		ts=`date -d "$OPTARG" +%F` ;;
	s)	# size (default: w=`tput cols`,h=w/4), e.g. `120,30`
		size=$OPTARG ;;
	l)	# layout (default "3,2", i.e. 3 rows, 2 cols)
		layout=$OPTARG ;;
	x)	# xrange arguments for all plots
		xrange="set xrange [$OPTARG]" ;;
	y)	# yrange arguments for each plot separately:
		# temp pressure signal humidity battery
		# e.g. '-10:40 900:1100 -90:-20 20:99 2300:3100' or
		# '20:22 999:1012 -90:-20 57:62 2900:2960' or only '20:22'
		eval $(echo $OPTARG | { read yt yp ys yh yb; echo yt=$yt yp=$yp ys=$ys yh=$yh yb=$yb; }) ;;
	n) 	# how long to put the xrange back from now
		# i.e. the from in `set xrange [from:now]`
		now=`date +%Y-%m-%dT%H:%M:%S`
		from=`date -d "$OPTARG" +%Y-%m-%dT%H:%M:%S`
		xrange="set xrange ['$from':'$now']"
		;;
	T)	# include Temperature plot
		show_T="y"
		;;
	P)	# include Pressure plot
		show_P="y"
		;;
	S)	# include Signal strength plot
		show_S="y"
		;;
	H)	# include Humidity plot
		show_H="y"
		;;
	B)	# include Battery plot
		show_B="y"
		;;
	A)	# include all plots; equivalent to -T -P -S -H -B (or -TPSHB)
		show_T="y"
		show_P="y"
		show_S="y"
		show_H="y"
		show_B="y"
		;;
	h)	# this help
		echo OPTIONS:
		sed -n '
			/p)/,/esac/ {
				/#/ p
			} ' $0
		exit 0
		;;
	esac
done

echo ys: $yt $yp $ys $yh $yb
echo size: $size
echo $xrange
file=${file:-`ls -t ~/dev/ruuviparse/logs/ruuvi-*-$ts|sed 1q`}
echo plotting from file $file


[ x$show_T = xy ] && T="
set title 'Temperature'
set yrange [${yt:-17:34.5}]
# set ylabel 'Celsius'
plot '$file' using 1:3 with dots
"

[ x$show_P = xy ] && P="
set title 'Pressure'
set yrange [${yp:-900:1100}]
# set ylabel 'hPa'
plot '$file' using 1:5 with dots
"

[ x$show_S = xy ] && S="
set title 'Signal strength'
set yrange [${ys:--100:-20}]
# set ylabel 'dB'
plot '$file' using 1:2  with dots
"

[ x$show_H = xy ] && H="
set title 'Humidity'
set yrange [${yh:-0:100}]
# set ylabel 'Percent'
plot '$file' using 1:4 with dots
"

[ x$show_B = xy ] && B="
set title 'Battery'
set yrange [${yb:-2000:3000}]
# set ylabel 'mV'
plot '$file' using 1:6 with dots
"

# plot 'logs/ruuvi-FE:D2:13:F2:EA:C1-2022-12-08' using 1:3 with line ls 3 lt 3 title "FE:D2:13:F2:EA:C1",
#      'logs/ruuvi-FF:6B:99:A9:49:11-2022-12-08' using 1:3 with line ls 3 lt 5 title "FF:6B:99:A9:49:11"

cat <<. | gnuplot --persist
set term dumb size $size nofeed # ansi256
#set grid
set xdata time
set timefmt "%Y-%m-%dT%H:%M:%S"
set multiplot layout $layout
$xrange
# set xtics rotate by 60
set format x "%H:%M"

unset key

$T

$P

$S

$H

$B

unset multiplot
.
